<?php

$app->group('/deploy', function() use ($app){
        $app->post('/php',function($request, $response){
                $reqMsg = 'Attempted Push UUID:'.$response->getHeader('HTTP_X_REQUEST_UUID').' on '.date('j.n.Y H:i:s')."\n";
                logRequest($reqMsg);

                $body = $request->getParsedBody();
                $branch = $body['push']['changes'][0]['new']['name'];

                if($branch == getenv('ENV_BRANCH'))
                {
                        $output = [];
                        $apiDir = getcwd();
                        $project = $body['repository']['project']['name'];
                        $repo = $body['repository']['name'];

                        if($project && $project !== '')
                                chdir("/var/www/html/$project/$repo");
                        else
                                chdir("/var/www/html/$repo");
                        
                        exec("/usr/bin/git checkout ".getenv('ENV_BRANCH')."; /usr/bin/git pull origin ".getenv('ENV_BRANCH')." 2>&1; composer install 2>&1",$output, $runStatus);
                        chdir($apiDir);
                        logRequest("ATTEMPTED PULL ON REPOSITORY: $repo \n RESULT: \n");
                        foreach ($output as $key => $value) {
                                # code...
                                logRequest("    ".$value."\n");
                        }
                        return $response->withJson(["status"=>$runStatus],200)->withHeader('Content-type', 'application/json');
                }
                else
                {
                        logRequest("NOT DEPLOY BRANCH: $branch \n");
                        return $response->withJson(["status"=>"Wrong Branch"],400)->withHeader('Content-type', 'application/json');
                }
        });

        $app->post('/angular',function($request, $response){
                $reqMsg = 'Attempted Push UUID:'.$response->getHeader('HTTP_X_REQUEST_UUID').' on '.date('j.n.Y H:i:s')."\n";
                logRequest($reqMsg);

                $body = $request->getParsedBody();
                $branch = $body['push']['changes'][0]['new']['name'];

                if($branch == getenv('ENV_BRANCH'))
                {
                        $output = [];
                        $apiDir = getcwd();
                        $project = $body['repository']['project']['name'];
                        $repo = $body['repository']['name'];

                        if($project && $project !== '')
                                chdir("/var/www/html/$project/$repo");
                        else
                                chdir("/var/www/html/$repo");

                        $deploy = exec("/usr/bin/git checkout ".getenv('ENV_BRANCH')."; /usr/bin/git pull origin ".getenv('ENV_BRANCH')." 2>&1;HOME=~ubuntu && sudo -u ubuntu /usr/bin/npm install 2>&1; ng build 2>&1;HOME=~www-data",$output, $runStatus);
                        chdir($apiDir);
                        logRequest("ATTEMPTED PULL ON REPOSITORY: $repo \n RESULT: ");
                        foreach ($output as $key => $value) {
                                # code...
                                logRequest("    ".$value."\n");
                        }
                        return $response->withJson(["status"=>$runStatus],200)->withHeader('Content-type', 'application/json');
                }
                else
                {
                        logRequest("NOT DEPLOY BRANCH: $branch \n");
                        return $response->withJson(["status"=>"Wrong Branch"],400)->withHeader('Content-type', 'application/json');
                }
        });
});

function logRequest($msg)
{
        // write log file
        file_put_contents('../deploy.log',$msg,FILE_APPEND);
}